## OBJETIVO

Pasta destinada a armazenar a documentação das funcionalidades do projeto

## REGRAS

A criação dos arquivos de documentação deverão possuir nome em caixa alta e extensão .md
A nomeação do arquivo deve abordar o escopo da documentação descrita no arquivo. Exemplo: ENVIRONMENT.md, que possui as informações de configuração do ambiente de desenvolvimento.

Para submeter as alterações, criações ou exclusões de arquivos de documentação é necessário utilizar o branch **documentation** e posteriomente criar um [Pull Request](https://br.atlassian.com/git/tutorials/making-a-pull-request) para o branch **master**