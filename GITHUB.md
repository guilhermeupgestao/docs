### Nomenclatura do branch

Ao iniciar a jornada de trabalho realizar atualizar o branch de acordo com os branch’s de homologação **homologacao** e produção **master**

````
git pull origin homologacao
git pull origin master
````

Quando for trabalhar em uma nova funcionalidade ou correção, criar um branch novo a partir do branch **master**

````
git checkout -b [ nome do branch ]
````

Por convenção utilizamos o seguinte formato para o nome do branch:

**ID do card Pipz:**  Para localizar o ID, abra o card e clique na URL, o ultimo número é o ID do card

**ex:** https://upgestao.pipz.io/pipz/boards/board/17/card/3438/

**Titulo do card:** É titulo dado na solicitação do card

**ex:** Implementação do CI / CD Bitbucket e Amazon

essas informações deveram estar em caixa alta, resultando no seguinte nome de branch

````
git checkout -b 3438_IMPLEMENTACAO_CI_CD_BITBUCKET_AMAZON
````

### Testes em ambiente de homologação e produção

Os testes devem preferencialmente utilizados no ambiente de homologação **v1-hmg** e **up-hmg**. Porém existe alguns casos que há a necessidade de executar testes no ambiente de produção **v1** e **up**. Para realizar o teste da sua implementação de funcionalidade ou correção siga os passos abaixo:

1. Acesse os servidores. É necessário accesar os dois servidores para realizar a atualização dos projetos devido ao balanceamento de carga.

````
ssh -i [endereço do arquivo UPWEBPHP.pem] ubuntu@18.228.124.152
ssh -i [endereço do arquivo UPWEBPHP.pem] ubuntu@18.229.69.140
````

2. Atualizando os projetos up-hmg e v1-hmg o comando a ser utilizado deverá ser: 

````
git pull origin homologacao
````

2.1 Para o projeto up-hmg é necessário executar o comando abaixo para rodar o gulp

````
gulp buildprod
````

3. Atualizando os projetos up e v1 o comando a ser utilizado deverá ser:

````
git pull origin master
````

3.1 Para o projeto up é necessário executar o comando abaixo para rodar o gulp

````
gulp buildprod
````

### Teste de funcionalidade isolada

Para realizar o teste de funcionalidade isolada. Será nessário executar os seguintes comandos.

1. O comando git fetch irá localizar os branchs remotos no repositório online ( Bitbucket )

````
git fetch
````

2. Fazer o checkout para o branch que deseja realizar o teste isolado

````
git checkout [ nome do branch ]
````

2.1 Caso o teste seja realizado no projeto up-hmg, não esquecer de rodar o comando abaixo:

````
gulp buildprod
````


[ TESTE DE FUNCIONALIDADE ISOLADA ]