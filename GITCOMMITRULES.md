# **Convenção para mensagem de Commit**

## Introdução

> Para a criação de um histórico de revisão útil, o time deverá estar de acordo com a convenção de mensagem de commit que define as três características a seguir:

### Estilo

Sintaxe de marcação, margens de contorno, gramática, letras maiúsculas e pontuação. Todas essas coisas remove o trabalho de adivinhação e torna tudo o mais simples possível. O resultado final dessa prática consiste em log's extremamente consistente que não só gera um prazer na leitura, mas  que realmente se transforma em uma base consistente de informação.

### Conteúdo

Que tipo de informação o corpo da mensagem de commit deverá conter ? Será mesmo necessário escrever algo no corpo da mensagem ? O que não deverá estar no corpo da mensagem ?

### Metadado

Como os rastreamento de id's, numeração de pull request e outros, deverão ser mencionados ?

## 7 Regras para uma boa mensagem de commit

1.  Separe o assunto da mensagem do corpo da mensagem saltando uma linha
2.  Limite o tamanho do assunto da mensagem em 50 caracteres
3.  Utilize letra maiúscula na primeira palavra do assunto da mensagem
4.  Não termine o assunto da mensagem com ponto final
5.  Use o modo imperativo para escrever o assunto da mensagem
6.  Limite o tamanho do texto no corpo da mensagem em 72 caracteres
7.  Use o corpo da mensagem para explicar o "oque" e "porque" e não "como"

```
Resumir as alterações em cerca de 50 caracteres ou menos

Texto mais detalhado com o número de 72 caracteres. Em alguns contextos a primeira linha é usada para descrever o assunto do commit e o resto do texto como o corpo.
A linha para separar o assunto do corpo é necessário ( á menos que você omita o texto do corpo do commit ) pois varias ferramentas como `log`, `short-log` e `rebase` podem ficar confuso se os dois estiverem juntos.

Explique o problema que o commit está resolvendo. Foque no "por que" você está fazendo essa alteração ao contrário do "como" ( Já que o código explica isso )
Existem efeitos colaterais ou outras consequências não intencionais com a mudança ?

Outros parágrafos podem ser escritos depois de pular linha

- Marcações como bullets, numbers são permitidas

Se for utilizado um rastreador de problemas, coloque referências a eles na parte inferior como abaixo:

Resolves: #123
See also: #456, #789
```

### 1\. Separe o assunto da mensagem do corpo da mensagem saltando uma linha

A partir do comando **git commit**

> Embora não seja obrigatório, é uma boa ideia começar a mensagem de commit com uma única linha curta (menos de 50 caracteres) resumindo a alteração, seguida por uma linha em branco e depois por uma descrição mais detalhada. O texto para a primeira linha em branco em uma mensagem de confirmação é tratado como o título de confirmação e esse título é usado em todo o Git. Por exemplo, Git-format-patch (1) transforma o commit em email, e usa o título na linha de assunto e o resto do commit no corpo.

Em primeiro lugar, nem todo cometimento requer um sujeito e um corpo. Às vezes, uma única linha é boa, especialmente quando a mudança é simples. Por exemplo:

```
Corrigir erro de digitação na introdução ao guia do usuário
```

Nada mais precisa ser dito; se o leitor se perguntar qual foi o erro de digitação, ela pode simplesmente dar uma olhada na alteração em si, ou seja, usar: git show ou git diff ou git log -p.

Se você está fazendo o commit de algo assim, é fácil usar a linha de comando com a opção -m para para fazer commit:

```
$ git commit -m "Corrigir erro de digitação na introdução ao guia do usuário"
```

No entanto, quando o commit merece um pouco mais de explicação e contexto, você precisa escrever o corpo. Por exemplo:

```
Finalizar o jogo durante a partida

O usuário tem a possibilidade de finalizar o Game durante a partida
clicando na opção finalizar disponibilizada na tela

1\. Salvar as informações atuais do jogo no relátorio
2\. Direcionar o usuário para a tela inicial
```

> Não é aconselhavel utilizar o comando **git commit -m ""** para escrever commit's que possua texto no corpo da mensagem. Para escrever commit's que possua texto no corpo da mensagem use o comando **git commit**

Para realizr pesquisa apenas da primeira linha do log utilize o comando **git log --oneline**

```
$git log --oneline

42e769 Finalizar o jogo durante a partida
```

Para agrupar os logs de commit por usuário utilize o comando **git shortlog**

```
$git shorlog

Guilherme Oliveira (2):
      Merge pull request #1 from guilhermefos/development
      Merge pull request #2 from guilhermefos/development

João Silva (1):
      Initial commit from Create React App
```

### 2\. Limite o assunto á 50 caracteres

50 caracteres não é um limite fixo, é apenas uma regra sugerível. Deixar o assunto nesse tamanho garante que ele seja legível e força quem está escrevendo, a pensar por um momento, sobre uma forma mais concisa de explicar o que está acontecendo.

### 3\. Escreva a primeira palavra do assunto em Letra maiúscula

Exemplo:

```
Correção do header da página
```

ao invés de

```
correção do header da página
```

### 4\. Não termine o assunto com ponto final

Exemplo:

```
Correção do header da página
```

ao invés de

```
Correção do header da página.
```

### 5\. Usar o modo imperativo para escrita do assunto

> O modo imperativo é o modo verbal pelo qual se expressa uma ordem, pedido, desejo, súplica, conselho, convite, sugestão, recomendação, solicitação, orientação, alerta ou aviso.

Exemplos:

*   Refatorar o subsistema X para legibilidade
*   Atualizar a documentação de introdução
*   Remover métodos depreciados
*   Versão de lançamento 1.0.0

Escrever dessa maneira pode ser um pouco estranho no começo. Estamos amigas acostumados a falar no modo indicativo, que é sobre reportar fatos. É por isso que as mensagens de commit geralmente acabam sendo escritas da seguinte forma:

*   Corrigido o erro com Y
*   Mudando o comportamento de X
*   Mais correções para coisas quebradas
*   Novos métodos de API

Para remover qualquer confusão, aqui vai uma regra simples para que o commit possa ser escrito sempre da maneira correta.

**Um assunto escrito da maneira correta deve ser capaz de completar a seguinte frase:**

*   Se aplicado, este commit vai "_o assunto aqui"_

Exemplos:

*   Se aplicado, este commit vai **_refatorar o subsistema X para legibilidade_**
*   Se aplicado, este commit vai **_atualizar a documentação de introdução_**
*   Se aplicado, este commit vai **_remover métodos depreciados_**
*   Se aplicado, este commit vai _**lançar a versão de lançamento 1.0.0**_

**Note como não funciona caso não esteja no formato imperativo**

*   Se aplicado, este commit vai _**corrigirá um bug com Y**_
*   Se aplicado, este commit vai **_mudará o comportamento de X_**
*   Se aplicado, este commit vai _**mais correções para coisas quebradas**_
*   Se aplicado, este commit vai _**novos métodos de API**_

_Lembre-se:_ Usar o modo imperativo é importante apenas no assunto. Estas restrições não são necessárias na escrita do corpo da mensagem.

**6\. Limite o corpo da mensagem á 72 caracteres**

Como o Git não formata o texto automaticamente. Quando você escrever o copo da mensagem do commit, você deve ter em mente que deverá manter as margens a direta do texto automaticamente.

**7\. Use o corpo da mensagem para explicar o** "_o que"_**e** "_por que"_**no lugar de** "_como"_

> _Como exemplo, segue o commit do Bitcoin Core, explicando** "o que"** e **"por que"** foi realizado a modificação no código fonte._

```git
commit eb0b56b19017ab5c16c745e6da39c53126924ed6
Author: Pieter Wuille <pieter.wuille@gmail.com>
Date:   Fri Aug 1 22:57:55 2014 +0200

 Simplify serialize.h's exception handling

   Remove the 'state' and 'exceptmask' from serialize.h's stream
   implementations, as well as related methods.

   As exceptmask always included 'failbit', and setstate was always
   called with bits = failbit, all it did was immediately raise an
   exception. Get rid of those variables, and replace the setstate
   with direct exception throwing (which also removes some dead
   code).

   As a result, good() is never reached after a failure (there are
   only 2 calls, one of which is in tests), and can just be replaced
   by !eof().

   fail(), clear(n) and exceptions() are just never called. Delete
   them.
```

Explicar o contexto, facilitará para os próximos desenvolvedores, entender o contexto da modificação, gerando uma economia de tempo para entendimento do código. Commits que não seguem essa abordagem, não geram mensagens uteis para consultas futuras e entendimento das modificações realizadas no código.

Na maioria dos casos não é necessário explicar "como" as coisas foram feitas, já que o código por si só informa o "como".

```
@author: Guilherme Oliveira
@email: guilherme@upgestao.com.br
```