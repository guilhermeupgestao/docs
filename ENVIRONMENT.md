[TOC]

# Configuração do ambiente via Docker

> Este guia tem o objetivo de auxiliar o desenvolvedor passo-a-passo na configuração do ambiente de desenvolvimento

## REQUISITOS

> Para rodar os projetos é necessário possuir os seguintes programas previamente instalados

*   Node JS - [nodejs.org/en](https://nodejs.org/en/)
*   NPM - [npmjs.com](https://docs.npmjs.com)
*   Gulp - [gulpjs.com](https://gulpjs.com/docs/en/getting-started/quick-start)
*   Docker -[docker.com](https://docs.docker.com)

## DOCKER

**[ DIRE'TORIO PARA ESPELHAMENTO ]:** é o diretório onde o projeto se encontra na máquina (fora do docker)

docker login [ user: frederasfc | password: trisoftmadagascarformula10 ] 

docker pull frederasfc/upgestao:v6

docker image list

docker run -d -p 80:80 -p 5432:5432 -p 27017:27017 -v **[ DIRETÓRIO PARA ESPELHAMENTO ]**:/var/www/ -ti -d **[ ID DA IMAGEM ]**

docker ps -a

docker exec -it **[ CONTAINER ID ]** /bin/bash

/etc/init.d/apache2 start

/etc/init.d/postgresql start

mongod --config /etc/mongodb.conf &

## V1 

composer install --ignore-platform-reqs