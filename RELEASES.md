### Release Upgestão v1.3.1 6 de Agosto de 2019

*   Adicionar os formatos de arquivos zip, dwg, xml e rar aos anexos do cliente

### Release Upgestão v1.2.1 29 de Julho de 2019

**Funcionalidades**

*   Adicionar a funcionalidade de envio de Nfs-e por email A implementação da funcionalidade, visa permitir que as notas fiscais de serviço sejam enviadas por email aos clientes. O envio ocorre via API plug-notas.
*   CFOP 1604 - Lançamento de Crédito
*   Atualizar as fases do pedido vai app mobile

**Correções**

*   Correção da funcionalidade para utilização do aparelho leitor de código de barras.

### Release Upgestão v1.1.0 24 de Julho de 2019

*   Atualização da tela de financeiro para versão 2.0

### Release Upgestão v1.0.0 23 de Julho de 2019

*   Correção da pesquisa por Descrição/Código Interno e Descrição/ Código Original
