[TOC]

Para fazer a criação da Nfe é realizado a busca para encontrar o registro das informações financeiras [VixFin]
través do número de lançamento enviado na requisição.

### Verificação do código de ação

É realizado a verificação do código de ação para identificar as ações listadas abaixo e retornado uma mensagem de erro:

1.  Caso não exista um código de ação
2.  Caso a ação esteja concluida ou cancelada
3.  Caso o número do documento esteja zerado ou vazio

```php
{
    'msg'           => $msg,
    'strMotivo' => $msg,
    'error'         => true
}
```

### Configuração Tributária

```
$this->_setSisEmp();
```

Após a verificação, é realizada a configuração tributária da empresa de acordo com as opções:

1.  REG_TRIB_LR
2.  REG_TRIB_LP
3.  REG_TRIB_DB

### Configuração NFe, NFCe

```
$this->_setNFCeNFe();
```

É realizada a configuração o Nfe ou Nfec de acordo com a especificação do documento por padrão a configuração é NFe

### Criação dos diretórios

```
$this->_criarDir();
```

É cria os diretórios de homologação e produção para salvar a Nfe ou Nfec

É verificado se a nota está sendo emitida para o exterior ou não

### Criação do XML

````
$this->montarXml()
````


Configura os parametros para criação do xml da nota e realiza a tentativa de criar o xml, caso ocorra
algum erro o mesmo é retornado para o cliente da requisição


### Assinatura do XML

```
$this->assinaXml();
```

Para assinar a nota, verifica-se o tipo de nota: A1 ou Vazio e realiza a tentativa de assinatura da nota,
caso ocorra algum erro na assinatura da nota retorna a mensagem de erro para o cliente da requisição

### Envio da NFe, NFCe

```
$this->_enviarNfe();
```

Realiza a tentativa de envio da Nfe e retorna as informações para o solicitante da requisição
ou retorna uma mensagem de erro para o solicitante da requisição
